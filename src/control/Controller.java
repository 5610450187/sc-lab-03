package control;
import view.Frame;
import model.HashingModel;

public class Controller {
	
	public static void main(String[] args){
		HashingModel has = new HashingModel();
		Frame frame = new Frame();
		
		has.Convert("http://www.cs.sci.ku.ac.th/~fscichj/ ");
		has.Dispart();
		has.Convert("https://twitter.com/doublepims");
		has.Dispart();
		has.Convert("https://docs.google.com");
		has.Dispart();
		has.Convert("http://stackoverflow.com");
		has.Dispart();
		has.Convert("http://www.hi5.com");
		has.Dispart();
		has.Convert("https://www.edmodo.com/");
		has.Dispart();
		has.Convert("http://en.wikipedia.org/wiki/Imperative_programming");
		has.Dispart();
		has.Convert("http://pantip.com/topic/33122874/story");
		has.Dispart();
		has.Convert("http://kampol.htc.ac.th");
		has.Dispart();
		has.Convert("https://www.youtube.com");
		has.Dispart();
		has.Convert("https://bitbucket.org/");
		has.Dispart();
		has.Convert("https://kitty.in.th/?s=imperative");
		has.Dispart();
		
		frame.setTextUrl(has.getList1(),has.getList2(),has.getList3(),has.getList4());
		
		
	}

}
