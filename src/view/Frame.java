package view;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class Frame extends JFrame {
  private JFrame frame;
  private JPanel panel;
  private JLabel label;
  private JTextArea text;
  
  
  
  public Frame(){
	  frame = new JFrame("URL GROUP");
	  frame.setSize(1000, 300);
	  frame.setLayout(new BorderLayout());
	  
	  panel = new JPanel();
	  
	  label = new JLabel();
	  
	  text = new JTextArea();
	  
	  frame.add(panel);
	  frame.add(label);
	  frame.add(text);
	  frame.setVisible(true);
	  frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
  }
  
  public void setTextUrl(ArrayList<String> list1,ArrayList<String> list2,ArrayList<String> list3,ArrayList<String> list4){
	  text.setText("Group0 :" +list1+ "\n" +
			  "Group1 :"  +list2+ "\n"+
			  "Group2 :"  +list3+ "\n"+
			  "Group3 :" +list4 );
	  
  }
} 